import { AccountingService } from './../../services/accounting.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };

  commonData: any = [];
  fieldData: any = [];

  constructor(
    private accountingService: AccountingService
  ) { }

  ngOnInit() {
    this.getCommonData();
    this.getFieldDetails();
  }

  getCommonData() {
    this.accountingService.getCommonData().subscribe(res => {
      this.commonData = res.data;
    });
  }

  getFieldDetails() {
    this.accountingService.getFieldDetailsData().subscribe(res => {
      this.fieldData = res.data;
    });
  }
}
