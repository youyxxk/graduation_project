import { DoTestService } from './../../../services/do-test.service';
import { QuestionsService } from './../../../services/questions.service';
import { ExamService } from './../../../services/exam.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { debounceTime, finalize } from 'rxjs/operators';
import { combineLatest, forkJoin } from 'rxjs';

@Component({
  selector: 'app-create-exams',
  templateUrl: './create-exams.component.html',
  styleUrls: ['./create-exams.component.scss'],
})
export class CreateExamsComponent implements OnInit {
  formGroup: FormGroup;
  fields = [];
  levels = [];
  exam: any = {};
  questions: Array<any> = [];
  beforeUpdateQs: Array<any> = [];

  role: 'create' | 'update';
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private fb: FormBuilder,
    private questionService: QuestionsService,
    private examService: ExamService,
    private doTestService: DoTestService,
    private dialogRef: MatDialogRef<CreateExamsComponent>
  ) {
    this.formGroup = this.fb.group({
      field: ['', Validators.required],
      level: ['', Validators.required],
      time: ['', Validators.required],
    });
    this.onFormChange();
  }

  ngOnInit() {
    this.fields = this.data.fields;
    this.levels = this.data.levels;
    this.exam = this.data.exam;
    this.role = this.data.role;
    this.formGroup.patchValue({
      field: this.exam.field.id,
      level: this.exam.level.id,
      questionNumber: this.exam.questionNumber,
      time: this.exam.time,
    });
  }

  private onFormChange() {
    combineLatest([
      this.formGroup.controls['field'].valueChanges,
      this.formGroup.controls['level'].valueChanges,
    ])
      .pipe(debounceTime(300))
      .subscribe(([field, level]) => {
        if (field && level) {
          this.getQuestions({ field, level });
        }
      });
  }

  createExam() {
    const selectedQs = this.getSelectedQs();
    if (this.formGroup.invalid || !selectedQs.length) {
      alert('Vui lòng điền đầy đủ các thông tin');
      return;
    }
    this.examService
      .createExam({...this.formGroup.getRawValue(), questionNumber: this.getSelectedQs().length})
      .subscribe((res: any) => {
        this.examService.addQuestions(
          selectedQs.map((x) => {
            return { examId: res.data.id, questionId: x.id };
          })
        )
        .pipe(finalize(() => this.dialogRef.close(true)))
        .subscribe();
      });
  }

  updateExam() {
    const selectedQs = this.getSelectedQs().map(o => o.id);
    if (this.formGroup.invalid || !selectedQs.length) {
      alert('Vui lòng điền đầy đủ các thông tin');
      return;
    }
    const deleted = this.beforeUpdateQs.filter(qs => !selectedQs.includes(qs));
    const inserted = selectedQs.filter(qs => !this.beforeUpdateQs.includes(qs));
    console.log(deleted);
    console.log(inserted);
    forkJoin([
      this.examService.updateExam({
        id: this.exam.id,
        ...this.formGroup.getRawValue(),
        questionNumber: this.getSelectedQs().length
      }),
      this.examService.addQuestions(
        inserted.map((x) => {
          return { examId: this.exam.id, questionId: x };
        })
      ),
      this.examService.deleteQuestions(
        deleted.map((x) => {
          return { examId: this.exam.id, questionId: x };
        })
      ),
    ])
      .pipe(finalize(() => this.dialogRef.close(true)))
      .subscribe();
  }

  getQuestions(params?: any) {
    if (this.role === 'create') {
      this.questionService.getQuestions(params).subscribe((res) => {
        this.questions = res.data.map((item) => {
          return { id: item.id, selected: false, content: item.content };
        });
      });
      return;
    }

    forkJoin([
      this.questionService.getQuestions(params),
      this.doTestService.getQuestions(this.data.exam.id),
    ]).subscribe(([all, selected]) => {
      this.questions = all.data.map((item) => {
        if (selected.data.find((s) => s.id === item.id)) {
          return { id: item.id, selected: true, content: item.content };
        }
        return { id: item.id, selected: false, content: item.content };
      });
      this.beforeUpdateQs = selected.data.map(item => item.id);
    });
  }

  getSelectedQs(): Array<any> {
    return this.questions.filter((x) => x.selected);
  }
}
