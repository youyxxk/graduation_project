import { CreateExamsComponent } from './create-exams/create-exams.component';
import { ConfirmDialogComponent } from 'app/shared/components/dialogs/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ExamService } from '../../services/exam.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { concat, forkJoin } from 'rxjs';

@Component({
  selector: 'app-admin-exam',
  templateUrl: './exams.component.html',
  styleUrls: ['./exams.component.scss'],
})
export class AdminExamComponent implements OnInit {
  displayedColumns: string[] = [
    'no',
    'field',
    'level',
    'questionNumber',
    'time',
    'action',
  ];
  dataSource: MatTableDataSource<any>;
  exams: Array<any>;
  fields = [];
  levels = [];
  searchForm: FormGroup;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private examService: ExamService,
    private fb: FormBuilder,
    private dialog: MatDialog
  ) {
    this.searchForm = this.fb.group({
      field: [],
      level: [],
      questionNumber: [],
      time: [],
    });
  }

  ngOnInit() {
    this.init();
    this.applyFilter();
  }

  private init() {
    forkJoin([
      this.examService.getAllExams(),
      this.examService.getFields(),
      this.examService.getLevels(),
    ]).subscribe(([exams, fields, levels]) => {
      this.initTable(exams);
      this.fields = fields;
      this.levels = levels;
    });
  }

  initTable(data: any) {
    this.exams = data.data;
    this.dataSource = new MatTableDataSource(this.exams);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getExams() {
    this.examService.getAllExams().subscribe((res: any) => {
      this.initTable(res);
    });
  }

  search(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  applyFilter() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((value) => this.examService.getAllExams(value))
      )
      .subscribe((res) => {
        this.initTable(res);
      });
  }

  clearFilter() {
    this.searchForm.reset();
  }

  createExam(role: 'create' | 'update', exam: any = { field: {}, level: {} }) {
    this.dialog
      .open(CreateExamsComponent, {
        disableClose: true,
        width: '80%',
        data: {
          fields: this.fields,
          levels: this.levels,
          role,
          exam,
        },
      })
      .afterClosed()
      .subscribe((value) => {
        if (value) {
          this.getExams();
        }
      });
  }

  deleteExam(id) {
    this.dialog
      .open(ConfirmDialogComponent, {
        data: { title: 'Bạn có chắc muốn xoá bài thi?' },
      })
      .afterClosed()
      .subscribe((value) => {
        if (value) {
          concat(
            this.examService.deleteExam(id),
            this.examService.getAllExams()
          ).subscribe((res) => {
            this.initTable(res);
          });
        }
      });
  }
}
