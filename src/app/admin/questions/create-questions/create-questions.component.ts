import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { Component, Inject, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CreateExamsComponent } from 'app/admin/exams/create-exams/create-exams.component';
import { ExamService } from 'app/services/exam.service';
import { QuestionsService } from 'app/services/questions.service';
import { finalize, take } from 'rxjs/operators';

@Component({
  selector: 'app-create-questions',
  templateUrl: './create-questions.component.html',
  styleUrls: ['./create-questions.component.scss']
})
export class CreateQuestionsComponent implements OnInit {
  formGroup: FormGroup;
  fields: any = [];
  levels: any = [];
  question: any = {};
  role: 'create' | 'update';
  @ViewChild('autosize') autosize: CdkTextareaAutosize;

 constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private fb: FormBuilder,
    private questionService: QuestionsService,
    private _ngZone: NgZone,
    private dialogRef: MatDialogRef<CreateExamsComponent>
  ) {
    this.formGroup = this.fb.group({
      field: ['', Validators.required],
      level: ['', Validators.required],
      content: ['', Validators.required],
      solutionA: ['', Validators.required],
      solutionB: ['', Validators.required],
      solutionC: ['', Validators.required],
      solutionD: ['', Validators.required],
      correct: ['A', Validators.required],
    })
  }

  ngOnInit() {
    this.fields = this.data.fields;
    this.levels = this.data.levels;
    this.question = this.data.question;
    this.role = this.data.role;
    if (this.data.role === 'update') {
      this.formGroup.patchValue({
        field: this.question.field.id,
        level: this.question.level.id,
        content: this.question.content,
        solutionA: this.question.solutionA,
        solutionB: this.question.solutionB,
        solutionC: this.question.solutionC,
        solutionD: this.question.solutionD,
        correct: this.question.correct,
      });
    }
  }

  createQuestion() {
    if (this.formGroup.invalid) {
      alert('Vui lòng điền đầy đủ các thông tin');
      return;
    }
    this.questionService.createQuestion(this.formGroup.getRawValue())
    .pipe(finalize(() => this.dialogRef.close(true)))
    .subscribe();
  }

  updateQuestion() {
    if (this.formGroup.invalid) {
      alert('Vui lòng điền đầy đủ các thông tin');
      return;
    }
    this.questionService.updateQuestion(this.question.id, this.formGroup.getRawValue())
    .pipe(finalize(() => this.dialogRef.close(true)))
    .subscribe();
  }

  triggerResize() {
    this._ngZone.onStable.pipe(take(1))
        .subscribe(() => this.autosize.resizeToFitContent(true));
  }
}
