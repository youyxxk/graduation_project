import { CreateQuestionsComponent } from './create-questions/create-questions.component';
import { ExamService } from './../../services/exam.service';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { QuestionsService } from '../../services/questions.service';
import { ConfirmDialogComponent } from 'app/shared/components/dialogs/confirm-dialog/confirm-dialog.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss'],
})
export class QuestionsComponent implements OnInit {
  displayedColumns: string[] = [
    'no',
    'content',
    'slnA',
    'slnB',
    'slnC',
    'slnD',
    'correct',
    'field',
    'level',
    'action',
  ];
  dataSource: MatTableDataSource<any>;
  questions: Array<any>;
  searchForm: FormGroup;
  fields: any = [];
  levels: any = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private questionService: QuestionsService,
    private dialog: MatDialog,
    private fb: FormBuilder,
    private examService: ExamService,
  ) {
    this.searchForm = this.fb.group({
      field: [],
      level: [],
    });
  }

  ngOnInit() {
    this.getQuestions();
    this.init();
    this.applyFilter();
  }

  private init() {
    forkJoin([
      this.examService.getFields(),
      this.examService.getLevels(),
    ]).subscribe(([fields, levels]) => {
      this.fields = fields;
      this.levels = levels;
    });
  }

  getQuestions() {
    this.questionService.getQuestions().subscribe((res) => {
      this.questions = res.data;
      this.dataSource = new MatTableDataSource(this.questions);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  search(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  applyFilter() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((value) => this.questionService.getQuestions(value))
      )
      .subscribe((res) => {
        this.questions = res.data;
        this.dataSource = new MatTableDataSource(this.questions);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  deleteQuestion(id) {
    this.dialog.open(ConfirmDialogComponent, {
      data: { title: 'Bạn có chắc muốn xoá câu hỏi?' },
    }).afterClosed().subscribe(value => {
      if (!value) { return; }
      this.questionService.deleteQuestions(id).subscribe(() => {
        this.getQuestions();
      });
    });
  }

  createQuestion(role: 'create' | 'update', question: any = {}) {
    this.dialog.open(CreateQuestionsComponent, {
      disableClose: true,
      width: '60%',
      data: {
        fields: this.fields,
        levels: this.levels,
        role,
        question,
      },
    }).afterClosed()
    .subscribe((value) => {
      if (value) {
        this.getQuestions();
      }
    });
  }

  uploadExcel(ev) {
    this.dialog.open(ConfirmDialogComponent, {
      data: { title: 'Upload excel file?' },
    }).afterClosed().subscribe(value => {
      if (!value) { return; }
      let workBook = null;
      let jsonData = null;
      const reader = new FileReader();
      const file = ev.target.files[0];
      reader.onload = (event) => {
        const data = reader.result;
        workBook = XLSX.read(data, { type: 'binary' });
        jsonData = workBook.SheetNames.reduce((initial, name) => {
          const sheet = workBook.Sheets[name];
          initial[name] = XLSX.utils.sheet_to_json(sheet);
          return initial;
        }, {});
        const questions = jsonData[Object.keys(jsonData)[0]];
        this.questionService.uploadExcel(questions).subscribe(() => {
          this.getQuestions();
        });
      }
      reader.readAsBinaryString(file);
    });
  }
}
