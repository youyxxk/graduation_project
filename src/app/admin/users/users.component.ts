import { UsersService } from './../../services/users.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ExamService } from 'app/services/exam.service';
import { ConfirmDialogComponent } from 'app/shared/components/dialogs/confirm-dialog/confirm-dialog.component';
import { forkJoin, concat } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { CreateExamsComponent } from '../exams/create-exams/create-exams.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  displayedColumns: string[] = [
    'no',
    'name',
    'phone',
    'address',
    'email',
    'createdDate',
    'action',
  ];
  dataSource: MatTableDataSource<any>;
  users: Array<any> = [];
  searchForm: FormGroup;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private userService: UsersService,
    private dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.userService.getAllUsers().subscribe((res: any) => {
      this.initTable(res);
    });
  }

  initTable(data: any) {
    this.users = data;
    this.dataSource = new MatTableDataSource(this.users);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  search(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  deleteUser(id) {
    this.dialog.open(ConfirmDialogComponent, {
      data: { title: 'Bạn có chắc muốn xoá người dùng này?' },
    }).afterClosed().subscribe((value => {
      if (value) {
        this.userService.deleteUser(id).subscribe(() => {
          this.getUsers();
        });
      }
    }));
  }
}
