import { RoutingState } from './services/routing-state.service';
import { Component, HostListener } from '@angular/core';
import { DoTestComponent } from './do-test/do-test.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private routingState: RoutingState) {
    this.routingState.loadRouting();
    // setInterval(() => {
    //   console.log(DoTestComponent.questions);
    // }, 3000);
  }

  @HostListener('window:beforeunload', ['$event'])
  handleBeforeUnload() {
    // if (DoTestComponent.questions) {
    //   DoTestComponent.getMark();
    // }
    // localStorage.setItem('questions', JSON.stringify(DoTestComponent.questions));
    // localStorage.setItem('method', JSON.stringify(DoTestComponent.getMark()));
  }
}
