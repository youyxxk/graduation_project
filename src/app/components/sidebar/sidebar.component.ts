import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class?: string;
    role: 'admin' | 'user',
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Tổng quan',  icon: 'dashboard', role: 'user'},
    { path: '/exams', title: 'Bài thi',  icon: 'content_paste', role: 'user'},
    { path: '/history', title: 'Lịch sử làm bài',  icon: 'library_books', role: 'user'},
    { path: '/user-profile', title: 'Hồ sơ',  icon: 'person', role: 'user'},
    // { path: '/notifications', title: 'Notifications',  icon: 'notifications', role: 'user'},
    // { path: '/icons', title: 'Icons',  icon: 'bubble_chart'},
    { path: '/admin/dashboard', title: 'Tổng quan',  icon: 'dashboard', role: 'admin'},
    { path: '/admin/exams', title: 'Quản lý đề thi',  icon: 'content_paste', role: 'admin'},
    { path: '/admin/questions', title: 'Quản lý câu hỏi',  icon: 'chat', role: 'admin'},
    { path: '/admin/users', title: 'Quản lý tài khoản',  icon: 'people', role: 'admin'},
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  userRole = localStorage.getItem('userRole');
  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem.role === this.userRole);
  }

  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
