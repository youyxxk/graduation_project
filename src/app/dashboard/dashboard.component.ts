import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  fields = [];

  constructor(private service: DashboardService, private router: Router) {}

  ngOnInit(): void {
    this.getAllFields();
  }

  getAllFields() {
    this.service.getFields().subscribe(data => {
      this.fields = data;
    });
  }
}
