import { ConfirmDialogComponent } from 'app/shared/components/dialogs/confirm-dialog/confirm-dialog.component';
import { localStorageTool } from '../shared/utilities/local-storage-tools';
import { ExamService } from '../services/exam.service';
import { DoTestService } from '../services/do-test.service';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ICheckDeactivate } from 'app/shared/interfaces/check-deactivate';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-do-test',
  templateUrl: './do-test.component.html',
  styleUrls: ['./do-test.component.css'],
})
export class DoTestComponent implements OnInit, ICheckDeactivate {
  questions = [];
  currentQuestion: any = {};
  questionIndex = 0;
  questionsNumber: any;
  exam = {};
  examId: number;
  isSubmitted = false;
  result = '';
  @ViewChild('showResult') showResult: TemplateRef<any>;

  constructor(
    private testService: DoTestService,
    private examService: ExamService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
  ) {
    window.onbeforeunload = (event: BeforeUnloadEvent) => {
      event.preventDefault();
      event.returnValue = false;
    }
  }

  ngOnInit() {
    this.route.params.subscribe(data => {
      this.examId = data.id;
      this.getExamInfo();
      this.getQuestions();
    });
  }

  checkDeactivate(): boolean | Observable<boolean> {
    if (this.isSubmitted) {
      return true;
    }

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {data: {title: 'Bạn có muốn thoát ?'}});
    dialogRef.afterClosed().subscribe(value => {
      if (value) {
        this.getMark();
      }
    });
  }

  getQuestions(): void {
    this.testService.getQuestions(this.examId)
    .subscribe({
      next: (data) => {
        this.questions = data.data.map(item => {
          return {selectedAnswer: '', ...item};
        });
        this.currentQuestion = this.questions[0];
        this.questionsNumber = new Array(this.questions.length);
      },
    });
  }

  getExamInfo() {
    this.examService.getExam(this.examId).subscribe(data => {
      this.exam = data.data;
    });
  }

  chooseQuestion(index: number) {
    if (index < 0 || index >= this.questions.length) {
      return;
    }
    this.questionIndex = index;
    this.currentQuestion = this.questions[this.questionIndex];
  }

  handleEvent(event) {
    if (event.action === 'done') {
      alert('Hết giờ làm bài');
      window.onbeforeunload = null;
      this.getMark();
    }
  }

  openModal() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {data: {title: 'Xác nhận nộp bài?'}});
    dialogRef.afterClosed().subscribe(value => {
      if (value) {
        this.getMark();
      }
    });
  }

  getMark() {
    const answers = this.questions.filter(value => value.selectedAnswer).map(question => {
      return {questionId: question.id, selectedAnswer: question.selectedAnswer};
    });
    this.examService.getMark({
      userId: localStorageTool.getUserData().id,
      examId: this.examId,
      answers
    }).subscribe(res => {
      this.result = res.result;
      this.dialog.open(this.showResult, {disableClose: true}).afterClosed().subscribe(() => {
        this.isSubmitted = true;
        this.router.navigate(['dashboard']);
      });
    }, error => {
      console.log(error);
    });
  }
}
