import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ExamService } from '../services/exam.service';
import { Component, OnInit } from '@angular/core';
import { ConfirmDialogComponent } from 'app/shared/components/dialogs/confirm-dialog/confirm-dialog.component';
@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.scss']
})
export class ExamComponent implements OnInit {
  exams = [];
  constructor(
    private examService: ExamService,
    private router: Router,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.getExams();
  }

  getExams() {
    this.examService.getAllExams().subscribe(data => {
      this.exams = data.data;
    })
  }

  openModal(exam) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {disableClose: true, data: {title: 'Xác nhận làm bài thi?'}});
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.router.navigate(['exams', exam.id, 'do-test']);
      }
    });
  }
}
