import { Observable } from 'rxjs';
import { ICheckDeactivate } from '../shared/interfaces/check-deactivate';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class DeactivateGuard implements CanDeactivate<ICheckDeactivate> {
  canDeactivate(
    component: ICheckDeactivate,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): boolean | Observable<boolean> {
    return component.checkDeactivate(currentRoute, currentState, nextState);
  }
}
