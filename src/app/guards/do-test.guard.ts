import { RoutingState } from '../services/routing-state.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DoTestGuard implements CanActivate {
  constructor(private router: Router, private routingState: RoutingState) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (this.routingState.getPreviousUrl().match(/^\/exams$/)) {
      return true;
    }
     this.router.navigate(['dashboard']);
    return false;
  }
}
