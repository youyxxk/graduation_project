import { localStorageTool } from '../shared/utilities/local-storage-tools';
import { HistoryService } from '../services/history.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css'],
})
export class HistoryComponent implements OnInit {
  userData: any;
  displayedColumns: string[] = ['no', 'exam', 'level', 'date', 'result'];
  dataSource: MatTableDataSource<any>;
  histories: Array<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private historyService: HistoryService) {
    this.userData = localStorageTool.getUserData();
  }

  ngOnInit() {
    this.getHistories(this.userData.id);
  }

  getHistories(userId?: number) {
    this.historyService.getHistories(userId).subscribe(data => {
      this.histories = data.data;
      this.dataSource = new MatTableDataSource(this.histories);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
