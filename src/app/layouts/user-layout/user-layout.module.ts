import { UsersComponent } from './../../admin/users/users.component';
import { CreateQuestionsComponent } from './../../admin/questions/create-questions/create-questions.component';
import { CreateExamsComponent } from './../../admin/exams/create-exams/create-exams.component';
import { AdminExamComponent } from './../../admin/exams/exams.component';
import { AdminDashboardComponent } from './../../admin/admin-dashboard/admin-dashboard.component';
import { UserLayoutRoutes } from './user-layout.routing';
import { ConfirmDialogComponent } from 'app/shared/components/dialogs/confirm-dialog/confirm-dialog.component';
import { ExamComponent } from '../../exam/exam.component';
import { DoTestComponent } from '../../do-test/do-test.component';
import { MatCardModule } from '@angular/material/card';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { HistoryComponent } from '../../history/history.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import { CountdownModule } from 'ngx-countdown';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatTabsModule} from '@angular/material/tabs';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import { QuestionsComponent } from 'app/admin/questions/questions.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
@NgModule({
  imports: [
    CommonModule,
    MatCheckboxModule,
    RouterModule.forChild(UserLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatCardModule,
    MatRadioModule,
    MatIconModule,
    MatDialogModule,
    CountdownModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatTabsModule,
    NgxChartsModule,
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    DoTestComponent,
    ExamComponent,
    HistoryComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    ConfirmDialogComponent,
    AdminDashboardComponent,
    AdminExamComponent,
    QuestionsComponent,
    CreateExamsComponent,
    CreateQuestionsComponent,
    UsersComponent,
  ]
})

export class UserLayoutModule {}
