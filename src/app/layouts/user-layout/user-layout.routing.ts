import { UsersComponent } from './../../admin/users/users.component';
import { QuestionsComponent } from './../../admin/questions/questions.component';
import { AdminExamComponent } from './../../admin/exams/exams.component';
import { AdminDashboardComponent } from './../../admin/admin-dashboard/admin-dashboard.component';
import { DoTestGuard } from '../../guards/do-test.guard';
import { DeactivateGuard } from '../../guards/deactivate.guard';
import { ExamComponent } from '../../exam/exam.component';
import { DoTestComponent } from '../../do-test/do-test.component';
import { Routes } from '@angular/router';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { HistoryComponent } from '../../history/history.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';

export const UserLayoutRoutes: Routes = [
    { path: '', redirectTo: 'dashboard' },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'user-profile', component: UserProfileComponent },
    { path: 'history', component: HistoryComponent },
    { path: 'icons', component: IconsComponent },
    { path: 'maps', component: MapsComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'upgrade', component: UpgradeComponent },
    {
      path: 'exams',
      children: [
        { path: '', component: ExamComponent },
        { path: ':id/do-test', component: DoTestComponent, canDeactivate: [DeactivateGuard], canActivate: [DoTestGuard] },
      ]
    },
    {
      path: 'admin',
      children: [
        { path: '', redirectTo: 'dashboard' },
        { path: 'dashboard', component: AdminDashboardComponent },
        { path: 'exams', component: AdminExamComponent },
        { path: 'questions', component: QuestionsComponent },
        { path: 'users', component: UsersComponent },
      ]
    },
];
