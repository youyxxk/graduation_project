import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountingService {
  url = `${environment.API}/accounting`;
  constructor(private http: HttpClient) {}

  getCommonData(): Observable<any> {
    return this.http.get(this.url);
  }

  getFieldDetailsData(): Observable<any> {
    return this.http.get(`${this.url}/field-details`);
  }
}
