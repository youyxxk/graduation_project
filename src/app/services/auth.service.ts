import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url = `${environment.API}/auth`;
  constructor(private http: HttpClient) {}

  register(value: any): Observable<any> {
    return this.http.post(`${this.url}/register`, value)
  }

  login(value: any): Observable<any> {
    return this.http.post(`${this.url}/login`, value)
  }

  logout(): Observable<any> {
    return this.http.post(`${this.url}/logout`, {});
  }
}
