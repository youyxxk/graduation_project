import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: "root",
})
export class DashboardService {
  private url = `${environment.API}/fields`;

  constructor(private http: HttpClient) {}

  getFields(): Observable<any> {
    return this.http.get(this.url);
  }
}
