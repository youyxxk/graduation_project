import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DoTestService {
  private url = `${environment.API}/exams`;
  constructor(private http: HttpClient) { }

  getQuestions(examId: number): Observable<any> {
    return this.http.get(`${this.url}/${examId}/questions`);
  }
}
