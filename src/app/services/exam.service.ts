import { createParams } from 'app/shared/utilities/create-param';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExamService {
  private url = `${environment.API}/exams`;

  constructor(private http: HttpClient) { }

  getAllExams(params?: any): Observable<any> {
    return this.http.get(this.url, {params: createParams(params)});
  }

  getFields(): Observable<any> {
    return this.http.get(`${environment.API}/fields`);
  }

  getLevels(): Observable<any> {
    return this.http.get(`${environment.API}/levels`);
  }

  getExam(id: number): Observable<any> {
    return this.http.get(`${this.url}/${id}`);
  }

  createExam(exam: {[key: string]: any}) {
    return this.http.post(this.url, exam);
  }

  updateExam(exam: {[key: string]: any}) {
    return this.http.put(this.url, exam);
  }

  addQuestions(details): Observable<any> {
    return this.http.post(`${environment.API}/details`, details);
  }

  deleteQuestions(details): Observable<any> {
    return this.http.post(`${environment.API}/details/delete`, details);
  }

  getMark(value: any): Observable<any> {
    return this.http.post(`${this.url}/mark`, value);
  }

  deleteExam(id: number): Observable<any> {
    return this.http.delete(`${this.url}/${id}`);
  }
}
