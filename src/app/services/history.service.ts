import { createParams } from 'app/shared/utilities/create-param';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root',
})
export class HistoryService {
  private url = `${environment.API}/histories`;
  constructor(private http: HttpClient) {}

  getHistories(userId?: number): Observable<any> {
    return this.http.get(this.url, {params: createParams({userId})});
  }
}
