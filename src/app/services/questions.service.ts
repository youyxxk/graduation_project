import { createParams } from 'app/shared/utilities/create-param';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root',
})
export class QuestionsService {
  private url = `${environment.API}/questions`;
  constructor(private http: HttpClient) {}

  getQuestions(params?: any): Observable<any> {
    return this.http.get(this.url, {params: createParams(params)});
  }

  deleteQuestions(id: number): Observable<any> {
    return this.http.delete(`${this.url}/${id}`);
  }

  createQuestion(question: any): Observable<any> {
    return this.http.post(`${this.url}`, question);
  }

  updateQuestion(id: number, question: any): Observable<any> {
    return this.http.put(`${this.url}/${id}`, question);
  }

  uploadExcel(questions: any): Observable<any> {
    return this.http.post(`${this.url}/excel`, questions);
  }
}
