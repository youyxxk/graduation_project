import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { unsupported } from '@angular/compiler/src/render3/view/util';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private url = `${environment.API}/users`;

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<any> {
    return this.http.get(this.url);
  }

  getUserProfile(id: number): Observable<any> {
    return this.http.get(`${this.url}/${id}`);
  }

  updateProfile(id: number, user: any): Observable<any> {
    return this.http.put(`${this.url}/${id}`, user);
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete(`${this.url}/${id}`);
  }
}
