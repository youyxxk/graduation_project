import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router,
  ) {
    this.loginForm = this.fb.group({
      username: [''],
      password: [''],
    });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.authService.login(this.loginForm.getRawValue()).subscribe(data => {
      localStorage.setItem('currentUser', JSON.stringify(data.data))
      localStorage.setItem('userRole', data.data.isAdmin ? 'admin' : 'user');
      const path = data.data.isAdmin ? 'admin' : '';
      this.router.navigate([path]);
    },
    error => {
      alert('Tên đăng nhập hoặc mật khẩu không đúng');
    });
  }

}
