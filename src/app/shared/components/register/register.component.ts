import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  constructor(
    private router: Router,
    private authService: AuthService,
    private fb: FormBuilder,
  ) {
    this.registerForm = this.fb.group({
      username: [''],
      password: [''],
      confirmPassword: [''],
      email: [''],
    })
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const data = this.registerForm.getRawValue();
    if (data.password !== data.confirmPassword) {
      return;
    }
    this.authService.register(data).subscribe(() => {
      this.router.navigate(['login']);
    },
    error => {
      console.log(error);
    });
  }

}
