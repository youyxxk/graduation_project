export function createParams(params: any): any {
  if (!params) { return null; }
  return Object.keys(params).reduce((acc, cur) => {
    if (params[cur]) {
      acc[cur] = params[cur].toString();
    }
    return acc;
  }, {});
}
