export class localStorageTool {
  private static userData = JSON.parse(localStorage.getItem('currentUser'));
  static getUserData() { return this.userData; }
}
