import { localStorageTool } from './../shared/utilities/local-storage-tools';
import { UsersService } from './../services/users.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ICheckDeactivate } from 'app/shared/interfaces/check-deactivate';
import { Component, HostListener, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { distinct, debounceTime } from 'rxjs/operators';
import { LocationStrategy } from '@angular/common';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
})
export class UserProfileComponent implements OnInit, ICheckDeactivate {
  user: any = {};
  form: FormGroup;
  changePasswordForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private userService: UsersService,
  ) {
    this.form = this.fb.group({
      fullName: [],
      phone: [],
      email: [],
      address: [],
    });
    this.changePasswordForm = this.fb.group({
      password: [],
      oldPassword: [],
      confirmPassword: [],
    });
    this.onChangeFormValue();
  }

  @HostListener('window:beforeunload', ['$event'])
  handleClose($event) {
      $event.returnValue = false;
  }

  ngOnInit() {
    this.getProfile();
  }

  checkDeactivate(): boolean | Observable<boolean> {
    return confirm('Các thay đổi có thể không được lưu lại?');
  }

  getProfile() {
    this.userService.getUserProfile(localStorageTool.getUserData().id)
    .subscribe((res: any) => {
      this.user =  res;
      this.form.patchValue(this.user);
    });
  }

  onChangeFormValue() {
    this.form.valueChanges.pipe(
      debounceTime(300),
      distinct(),
    ).subscribe();
  }

  updateProfile() {
    this.userService.updateProfile(this.user.id, this.form.getRawValue())
    .subscribe(() => this.getProfile());
  }

  changePassword() {
    const data = this.changePasswordForm.getRawValue();
    if (data.password !== data.confirmPassword) {
      alert('Mật khẩu không khớp');
      return;
    }
    this.userService.updateProfile(this.user.id, {oldPassword: data.oldPassword, password: data.password})
    .subscribe(() => {
      alert('Thay đổi mật khẩu thành công');
      this.changePasswordForm.reset();
    }, () => {
      alert('Mật khẩu không chính xác');
    });
  }
}
